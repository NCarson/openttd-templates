
class OBMap {

 static function PickRandomTile() {
	local w = AIMap.GetMapSizeX()
	local h = AIMap.GetMapSizeY()
	local x = floor(CoreUtils.Rand.random()*w).tointeger()
	local y = floor(CoreUtils.Rand.random()*h).tointeger()
	local tile = AIMap.GetTileIndex(x, y)
	return tile
}

}

/*
		for (x=0; x<AIMap.GetMapSizeX(); x++) {
			if (found)
				break
			for (y=0; y<AIMap.GetMapSizeY(); y++) {
				local key = AIMap.GetTileIndex(x, y)
				if (AITile.IsBuildable(key) 
					&& (AITile.GetSlope(key) == AITile.SLOPE_FLAT)) {
					tile = key
					found = true
					break
				}
			}
		}
		if (tile == null) {// I suppose this could happen in contrived situations
			return null
*/

