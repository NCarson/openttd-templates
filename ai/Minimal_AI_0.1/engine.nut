
class OBEngine {

	static function GetEngines () { //TODO check IsRailTypeAvailable would fix this nonsense
		local a = []
		local out = []
		local powered = []
		local rt = Set()
		foreach (key, _ in AIEngineList(AIVehicle.VT_RAIL)) {
			if (!AIEngine.IsValidEngine(key))
				continue
			// wagons will still be buildable even if the rail type does not exist yet
			if (!AIEngine.IsBuildable(key)) 
				continue
			a.push(key)
			if (!AIEngine.IsWagon(key))
				powered.push(key)
		}
		foreach (engine in powered) { 
			rt.add(AIEngine.GetRailType(engine))
		}
		local rt = OBEngine.GetRailTypes(powered)
		// see if we have a engine to pull the wagon with right rt
		foreach (vehicle in a) {
			if (rt.contains(AIEngine.GetRailType(vehicle)))
				out.append(vehicle)
		}
		return out
	}

	static function GetLength(key) {
	//XXX make sure we dont call this until the event loop. (segfaults in Init)
	// BUG I cannot believe that you have to build a vehicle to get the length
		//local mode = AITestMode()
		//local company = AICompanyMode(0)
		local n = 0	
		local tile, depot
		AIRail.SetCurrentRailType(0) // make sure railtype is set before building depot
		while (!depot) {
			n++
			if (n>500) {
				AILog.Warning("Could not find buildable tile for OBEngine.GetLength")
				return null
			}
			tile = OBMap.PickRandomTile()
			if (!AITile.IsBuildable(tile))
				continue
			local demo = AITile.DemolishTile(tile)
			local x = AIMap.GetTileX(tile)
			local y = AIMap.GetTileY(tile)

			depot = AIRail.BuildRailDepot(tile, AIMap.GetTileIndex(x-1,y))
			print(depot)
			print(x)
			print(y)
		}
		print(AIEngine.GetName(key))
		local cargo = AIEngine.GetCargoType(key)
		print(AICargo.GetName(cargo))
		local vehicle = AIVehicle.BuildVehicleWithRefit(tile, key, cargo)
		print(AIVehicle.GetBuildWithRefitCapacity(key, tile, cargo))
		print(AIVehicle.IsValidVehicle(vehicle))
		//print(AIRail.IsRailDepotTile(tile))
		//print(AIEngine.IsValidEngine(key))
		//print(AICompanyMode.IsValid()) // not in openttd-13
		print(AIVehicle.GetLength(vehicle))
	}

	static function GetRailTypes(keys) {
		local rt = Set()
		foreach (k in keys) { 
			rt.add(AIEngine.GetRailType(k))
		}
		return rt
	}

	static function KeysToNames(keys) {
		return Array.map(keys, AIEngine.GetName)
	}

	static function GetWagonsByCargo(keys, cargo) {
		local a = []
		foreach (key in keys)
			if (AIEngine.CanRefitCargo(key, cargo))
				a.push(key)
		return a
	}

	key = null
	name = null
	rail_type = null
	max_speed = null
	price = null
	running_cost = null
	horsepower = null
	tractive_effort = null // kN
	cargo_key = null
	weight = null // vehicle tons
	capacity = null // cargo tons
	cargo_weight = null
	max_age = null // days
	//reliability
	
	constructor(key) {
		this.key = key
		this.name = AIEngine.GetName(key)
		local rt = AIEngine.GetRailType(key)
		this.rail_type = AIRail.GetName(rt)
		this.max_speed = AIEngine.GetMaxSpeed(key)
		this.price = AIEngine.GetPrice(key)
		this.running_cost = AIEngine.GetRunningCost(key)
		this.horsepower = AIEngine.GetPower(key)
		this.tractive_effort = AIEngine.GetMaxTractiveEffort(key)
		this.cargo_key = AIEngine.GetCargoType(key)
		this.capacity = AIEngine.GetCapacity(key)
		this.weight = AIEngine.GetWeight(key)
		this.cargo_weight = AICargo.GetWeight(this.cargo_key, this.capacity)
		this.max_age = AIEngine.GetMaxAge(key)

		if (this.capacity == -1)
			this.capacity = 0
		if (this.cargo_weight == -1)
			this.cargo_weight = 0

		if (this.horsepower == -1)
			this.horsepower = null
		if (this.tractive_effort == -1)
			this.tractive_effort = null
		if (this.max_age == -1)
			this.max_age = null

			print(this.name)
		if (this.rail_type == "Railroad construction"){
			/*
			print("")
			print(this.weight)
			print(this.weight + this.cargo_weight)
			print(this.capacity)
			print(this.loaded_weight)
			print(this.horsepower)
			print(this.tractive_effort)
			print(this.max_speed)
			print(this.price)
			print(this.running_cost)
			*/
		}
	}
}
