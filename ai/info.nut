/*
https://www.tt-forums.net/viewtopic.php?f=65&t=62163
 * Copyright (C) 2012-2013  Leif Linse
 *
 * MinimalAI is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License
 *
 * MinimalAI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MinimalAI; If not, see <http://www.gnu.org/licenses/> or
 * write to the Free Software Foundation, Inc., 51 Franklin Street, 
 * Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

//https://docs.openttd.org/gs-api/
class FMainClass extends AIInfo {
	function GetAuthor()		{ return "[Insert your name here]"; }
// put your api version here
// Bananas server does not have a way to check your internal api version
// so you will probably be better to put it here
	function GetName()			{ return "Minimal AI 0.1"; } 
	function GetDescription() 	{ return "Minimal AI is a Game Script boilerplate"; }
	function GetVersion()		{ return 1; }
	function GetDate()			{ return "2013-11-05"; }
	function CreateInstance()	{ return "MainClass"; } // your main entry point class in main.nut
	function GetShortName()		{ return "MINI"; } // Replace this with your own unique 4 letter string
	function GetAPIVersion()	{ return "1.3"; } // This is openttd's api version! see the docs
	//function GetURL()			{ return ""; }

	function GetSettings() {
		AddSetting({name = "log_level", description = "Debug: Log level (higher = print more)", easy_value = 3, medium_value = 3, hard_value = 3, custom_value = 3, flags = CONFIG_INGAME, min_value = 1, max_value = 3});
		AddLabels("log_level", {_1 = "1: Info", _2 = "2: Verbose", _3 = "3: Debug" } );
	}
}

RegisterAI(FMainClass());
