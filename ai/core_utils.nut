CoreUtils <- {}

CoreUtils._in_openttd <- false // will be set later in check for system clock function

//TODO add variadic support


CoreUtils.pprint <- function(thing, end="\n") {
/** pretty printer for different data structures */
	switch(typeof thing) {
		case "array":
			print(CoreUtils.Array.tostring(thing))
			break
		case "table":
			print(CoreUtils.Table.tostring(thing))
			break
		default:
			print(thing)
		}
		print(end)
		return "" //FIXME sometimes function prints null without the blank return
}

CoreUtils.rootkeys <- function(table=null) {
/** Introspects slots of tables */

	if (table == null) {
		if (CoreUtils._in_openttd) {
			throw "there is no access to the root table in openttd"
		}
		table = getroottable()
	}
	local keys = Table.keys(table)
	keys.sort()
	return keys
}

/*
class String {
	static function join(sep, arr) {
		out = ""
		foreach(thing in arr) {
			out += thing
		}
	}
}
*/



/* this wont work in openttd; no getrootable
class Util {
	static function defined(name) {
		return Array.contains(Table.keys(getroottable()), name)
	}
}
*/

class CoreUtils.Array {
/** static class to handle missing standard library functionality */

	static _test_script = @"

		::Array <- CoreUtils.Array

		local a = [1,2,3]
		pprint(a)
		pprint(Array.count(a, 1))
		pprint(Array.count(a, 4))
		pprint(Array.contains(a, 1))
		pprint(Array.contains(a, 4))
		pprint(Array.min(a))
		pprint(Array.max(a))
		pprint(Array.unique([7,1,1,2,8,2,3,4,4,4]))
		pprint(Array.unique([]))
		pprint(Array.unique([1]))
		pprint(Array.unique([1, 1]))
		pprint(Array.column([[1,2,3],[1,2,3]], 0))
		foreach (row in (Array.select([[1,2,4],[1,2,3]], 2, 4))) {
			pprint(row)
		}

		local function istwo(v, i)
			return v==2
		local function isidxtwo(v, i)
			return i==2
		pprint(Array.filter(a, istwo))
		pprint(Array.filter(a, isidxtwo))

		local function addone(v, i, arr) return v + 1
		local function giveidx(v, i) return i
		local function giveval(v) return v
		pprint(Array.map(a, addone))
		pprint(Array.map(a, giveidx))
		pprint(Array.map(a, giveval))

		"

	static function tostring(arr) {
	/** pretty prints the contents of an array (it does not recurse nested structures) */
		if (arr == null)
			return null
		local out = "[ "
		foreach (i, piece in arr) {
			out += piece
			if (i < arr.len()-1) 
				out += ", "
		}
		out += " ]"
		return out
	}

	static function min(arr) {
	/** returns minimum value of array */
		local tmp = arr.slice(0)
		tmp.sort()
		return tmp[0]
	}

	static function max(arr) {
	/** returns maximum value of array */
		local tmp = arr.slice(0)
		tmp.sort()
		tmp.reverse()
		return tmp[0]
	}

	static function unique(arr) {
	/** returns sorted unique values of an array */
		if (arr.len()==0)
			return []
		local a = arr.slice(0)
		a.sort()
		local out = []
		local j = 0
		out.append(a[0])
		foreach(val in a.slice(1)) {
			if(val != out[j]) {
				out.append(val)
				j++
			}
		}
		return out
	}

	static function contains(arr, val) {
	/**FIXME this should be removed */
		foreach(v in arr) {
			if (val == v) return true
		}
		return false
	}

	static function count(arr, val) {
	/** returns the count of occurences in the array */
		local n = 0
		foreach(v in arr) {
			if (val == v) n += 1
		}
		return n
	}

	static function filter(arr, func) {
	/** returns true values for the func of form: func(val, index). Also a method of builtin array but the functor args are reversed?! */

		local a = []	
		foreach (i, val in arr)
			if (func(val, i))
				a.push(val)
		return a
	}
	static function map(arr, func) {
	/** returns new array transformed by function of the form: func(item_value, [item_index], [array_ref]) */
	// TODO use function.getinfos to see how many args it take
		local a = []
		local info = func.getinfos()
		local n

		if (Array.contains(CoreUtils.Table.keys(info), "paramscheck")) { // if we are using an old / embedded version of squirrel
			n = info.paramscheck - 1
		} else {
			n = info.parameters.len() - 1 - info.defparams.len()
		}

		foreach (i, val in arr) {
			if (n == 1) {
				a.push(func(val))
			} else if (n == 2) {
				a.push(func(val, i))
			} else if (n == 3) {
				a.push(func(val, i, arr))
			} else {
				a.push(func(val)) // let it pop
			}
		}
		return a
	}

	// two dim arrays
	static function column(arr, index) {
	/** returns an array composed of the same index of each row from a two-dim array*/
		local a = []
		foreach (row in arr) {
			a.push(row[index])
		}
		return a
	}

	static function select(arr, idx, value) {
	/** selects rows from a two-dim array where the value matches to the row val given by idx*/
		local out = []
		foreach (row in arr) {
			if (row[idx] == value) out.append(row)
		}
		return out
	}
}
class CoreUtils.Math {
/** basic math utilities */

	static _test_script = @"
		pprint(CoreUtils.Math.round(-0.8)) // -1
		pprint(CoreUtils.Math.round(0.2)) //  0
		pprint(CoreUtils.Math.radians(180))
		pprint(CoreUtils.Math.degrees(3.142))
		"
	static function round(x) {
	/** rounds a float to the nearest integer */
		if (x < 0.0) {
			return ((x - 0.5).tointeger())
		} else {
			return (x + 0.5).tointeger()
		}
	}

	static function radians(x) {
	/** returns radians given degrees */
		return x * (PI/180)
	}

	static function degrees(x) {
	/** returns degrees given radians */
		return x * (180/PI)
	}
}


class CoreUtils.Rand {
/** Basic random functions */

	_RAND_MAX = pow(2, 16) / 2 - 1

	static _test_script = @"
		::Rand <- CoreUtils.Rand
		local a = [1,2,3,4,5,6]
		local seed = 1

		//seeded
		Rand.srand(seed)
		pprint(Rand.rand())
		pprint(Rand.random(seed))
		Rand.shuffle(a, seed); pprint(pprint(a))

		//not seeded
		//pprint("")
		Rand.srand(time()) //FIXME not in openttd
		pprint(Rand.rand())
		pprint(Rand.random())
		Rand.shuffle(a); 
		pprint(pprint(a))
		foreach(v in a)
		Rand.shuffle(a); pprint(pprint(a))
		"
	
	static function srand(seed) {
	/** sets the random seed for the whole class  */
		//seed = seed % Rand._RAND_MAX
		CoreUtils.Rand._SEED <- seed.tointeger();
	}

	static function rand() {
		/** FIXME  */
		// XXX RAND_MAX assumed to be 32767 
		CoreUtils.Rand._SEED <- CoreUtils.Rand._SEED * 1103515245 + 12345;
		return abs((CoreUtils.Rand._SEED/65536) % 32768);
	}

	static function random(seed=null) { // scale rand to float (0-1)
		/** FIXME  */
		if (seed != null) CoreUtils.Rand.srand(seed)
		return CoreUtils.Rand.rand() / CoreUtils.Rand._RAND_MAX.tofloat()
	}

	static function shuffle(arr, seed=null) { // shuffle array idxs
		/** randomly shuffles an array in place */

		if (seed != null) CoreUtils.Rand.srand(seed)

		local n = arr.len()
		foreach (i, val in arr) {
			local j = i + CoreUtils.Rand.rand() / (CoreUtils.Rand._RAND_MAX / (n - i) + 1 )
			local tmp = arr[j]
			arr[j] = arr[i]
			arr[i] = tmp
		}
	}

}


// openttd has scrubbed time from squirrl. Its just too powerful. Dubmasses.
//if (Util.defined("time")) { // we cant use defined as openttd has scrubbed getroottable. fuckers!
try { //XXX check for openttd first.
	// Even if the error is caught correctly openttd makes a big fuss and 
	// prints out an error msg in the debug console and log.
	CoreUtils.Rand._SEED <- abs(GSBase.Rand())
	CoreUtils._in_openttd = true
} catch(err) { //openttd scrubbed time from squirrel?!
	CoreUtils.Rand._SEED <- time()
}


class CoreUtils.Set { // TODO add the set operators
/** Adds a set class where values are gauranteed to be unique in a container */

	_data = null

	static _test_script = @"
		local s = CoreUtils.Set([1,2,3,3])
		pprint(s)
		pprint(s.tostring())
		pprint(s.len())
		s.add(5); pprint(s)
		pprint(s.contains(6))
		pprint(s.contains(5))
		s.remove(5); pprint(s)
		foreach(val in s) {
			pprint(val)
		}
		s.clear()
		pprint(s.tostring())
		"

	constructor(arr=[]) {

		this._data = []
		foreach (val in arr) {
			if (this._data.find(val) == null)
				this._data.append(val)
		}
	}

	function _tostring() {
		if (this._data == null)
			return null
		local out = "( "
		foreach (i, piece in this._data) {
			out += piece
			if (i < this._data.len()-1) 
				out += ", "
		}
		out += " )"
		return out
	}

	function _get(idx) {
		return this._data[idx]
	}

	function _nexti(prev) {
		if (this._data.len() == 0)
			return null
		if (prev == null)
			return 0
		if (prev == this._data.len() - 1)
			return null
		return prev + 1
	}

	function values() {
	/** returns the set values as an array */
		return this._data.slice(0)
	}

	function len() { return this._data.len() }
	/** returns the count of values in the set */

	function add(val) {
	/** adds a value to the set if it does not already contain it */
		if (!this.contains(val))
			this._data.append(val)
	}

	function contains(val) {
	/** returns true if the value is in the set*/
		foreach (v in this._data)
			if (v == val)
				return true
		return false
	}

	function remove(value) {
	/** removes the value from the set*/
		foreach (i, val in this._data) {
			if (val == value) {
				this._data.remove(i)
				return
			}
		}
		throw "key error: " + value + " not in set"
	}

	function clear() {
	/** removes all the values from the set*/
		this._data = []	
	}
}

class CoreUtils.Table {
	/** missing static utility functions for the table datatype*/

	static _test_script = @"
		local t = {a=1, b=2, c=3}
		pprint(CoreUtils.Table.keys(t))
		pprint(CoreUtils.Table.values(t))
		foreach( item in (CoreUtils.Table.items(t))) {
			pprint(item)
		}
		"

	static function tostring(table) {
	/** pretty prints the values in the table */
		local out = "{"
		foreach (key, val in table)
			out += " " + key + "=" + val
		out += " }"
		return out
	}

	static function keys(table) { //XXX the doc says this exists, but it does not seem to exists
	/** return the keys of a table as an array */
		local out = []
		foreach (key, val in table)
			out.append(key)
		return out
	}

	static function values(table) {//XXX the doc says this exists, but it does not seem to exists 
	/** return the values of a table as an array*/
		local out = []
		foreach (key, val in table)
			out.append(val)
		return out
	}

	static function items(table) {
	/** return the keys and values of a table in the form of [[k,v],...] */
		local out = []
		foreach (key, val in table)
			out.append([key, val])
		return out
	}

}


class CoreUtils.TestHarness {
	_name = null

	constructor(name) {
		this._name = name
	}

	function run(str) {
		::pprint <- CoreUtils.pprint
		print("<<<\n" + str + "\n")
		print(">>>\n")
		compilestring(str)()
	}

	function test(name, cls) {
		print("\nTesting " + name + "\n")
		this.run(cls._test_script)
	}

	static function _test() {
		local harness = CoreUtils.TestHarness("")
		harness.test("Math", CoreUtils.Math)
		harness.test("Set", CoreUtils.Set)
		harness.test("Table", CoreUtils.Table)
		harness.test("Array", CoreUtils.Array)
		harness.test("Rand", CoreUtils.Rand)
	}
}
//CoreUtils.TestHarness._test()
